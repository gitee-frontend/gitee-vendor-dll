const path = require('path')
const entry = require('./entry')

function createConfig(mode = process.env.NODE_ENV) {
  const library = {}
  const cwd = process.cwd()
  const outputPath = path.resolve(__dirname, 'dist', mode === 'production' ? 'production' : 'development')
  Object.keys(entry).forEach((name) => {
    const manifestPath = path.join(outputPath,`${name}-manifest.json`)

    library[name] = {
      referenceConfig: {
        context: path.join(cwd, 'config'),
        manifest: require(manifestPath)
      }
    }
  })
  return {
    manifestPath: path.join(outputPath, `manifest.json`),
    copyConfig: {
      from: outputPath,
      to: path.join(cwd, 'public/webpacks'),
      ignore: ['*.json']
    },
    library
  }
}

module.exports = {
  createConfig
}
