# [3.0.0-beta.2](https://gitee.com/gitee-frontend/gitee-vendor-dll/compare/v3.0.0-beta.1...v3.0.0-beta.2) (2020-05-29)


### Features

* 移除 echarts ([c398031](https://gitee.com/gitee-frontend/gitee-vendor-dll/commits/c3980318f322251f6724b18b4151765349872827))



# [3.0.0-beta.1](https://gitee.com/gitee-frontend/gitee-vendor-dll/compare/v3.0.0-beta.0...v3.0.0-beta.1) (2020-05-13)


### Bug Fixes

* DllPlugin() 生成的 manifest.json 无法给 StatsPlugin() 使用 ([8da1b0f](https://gitee.com/gitee-frontend/gitee-vendor-dll/commits/8da1b0f28ec78209866e31fc14331add0b72ba2f))



# [3.0.0-beta.0](https://gitee.com/gitee-frontend/gitee-vendor-dll/compare/v3.0.0-beta...v3.0.0-beta.0) (2020-04-16)


### Bug Fixes

* correct the dependence on echarts ([4ad47a8](https://gitee.com/gitee-frontend/gitee-vendor-dll/commits/4ad47a830d9a7887a00f176adee1fe700f2b6d07))



# [3.0.0-beta](https://gitee.com/gitee-frontend/gitee-vendor-dll/compare/v2.0.0...v3.0.0-beta) (2020-04-16)


### Features

* add entry point for echarts ([083e12b](https://gitee.com/gitee-frontend/gitee-vendor-dll/commits/083e12b95b0140a66315ee7d77c59c61fc277a57))


### BREAKING CHANGES

* config.manifest has been removed, use config.manifestPaths instead



# [2.0.0](https://gitee.com/gitee-frontend/gitee-vendor-dll/compare/v1.2.2...v2.0.0) (2020-01-16)


### Features

* 增加 mode 参数 ([47416b3](https://gitee.com/gitee-frontend/gitee-vendor-dll/commits/47416b346d1e7e0a4347aca6ce2be97c0fd8a1e1))


### BREAKING CHANGES

* createDllConfig -> createCofnig



## [1.2.2](https://gitee.com/gitee-frontend/gitee-vendor-dll/compare/v1.2.1...v1.2.2) (2019-12-24)


### Bug Fixes

* Vue 未识别出生产模式 ([0c5a829](https://gitee.com/gitee-frontend/gitee-vendor-dll/commits/0c5a8298ac66452c6bf62951f93b24dd11475d3c))



## [1.2.1](https://gitee.com/gitee-frontend/gitee-vendor-dll/compare/v1.2.0...v1.2.1) (2019-12-13)


### Bug Fixes

* dll 引用路径错误 ([9fae2df](https://gitee.com/gitee-frontend/gitee-vendor-dll/commits/9fae2dfd81f7fd2a9164dda45b2b3ba62079192c))



# [1.2.0](https://gitee.com/gitee-frontend/gitee-vendor-dll/compare/v1.1.0...v1.2.0) (2019-12-13)


### Features

* 调整 referenceConfig 的结构 ([550112e](https://gitee.com/gitee-frontend/gitee-vendor-dll/commits/550112ecc531a376fd26eb7bb8e43e916428570d))



# [1.1.0](https://gitee.com/gitee-frontend/gitee-vendor-dll/compare/v1.0.0...v1.1.0) (2019-12-13)


### Features

* 移除多余的依赖 ([045edb9](https://gitee.com/gitee-frontend/gitee-vendor-dll/commits/045edb9cec18de9290a02c137ef8634ba3898ced))



# 1.0.0 (2019-12-13)



