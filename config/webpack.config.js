const path = require('path')
const webpack = require('webpack')
const StatsPlugin = require('stats-webpack-plugin')
const StripDllManifestWebpacPlugin = require('strip-dll-manifest-webpack-plugin')
const entry = require('../entry')

const libraryName = '[name]_lib'

function generateConfig(mode) {
  // StatsPlugin() 的缓存，用于让该插件将多个 manifest 数据合并到 manifest.json 中
  const statsCache = {}
  const outputPath = path.join(__dirname, '..', 'dist', mode)
  // 为 entry 中的每项生成配置，避免因 entry 项改动后导致全部输出文件的 hash 值改变
  return Object.keys(entry).map((key) => ({
    mode,
    entry: {
      [key]: entry[key]
    },
    output: {
      path: outputPath,
      publicPath: '/webpacks/',
      filename: `${libraryName}-[chunkhash].js`,
      library: libraryName
    },
    devtool: 'none',
    performance: {
      hints: false
    },
    plugins: [
      new StatsPlugin('manifest.json', {
        chunkModules: true,
        source: false,
        chunks: false,
        modules: false,
        assets: true
      }, statsCache),
      new webpack.DllPlugin({
        name: libraryName,
        // 主仓库的模块路径是相对于 config 目录，例如：
        // ../node_modules/vue/dist/vue.js
        // 这里将 context 设置为当前目录，以和主仓库中的模块引入路径一致
        context: __dirname,
        path: path.join(outputPath, '[name]-manifest.json')
      }),
      new StripDllManifestWebpacPlugin({
        manifestPath: path.join(outputPath, `${key}-manifest.json`)
      })
    ]
  }))
}

module.exports = [
  ...generateConfig('development'),
  ...generateConfig('production')
]
