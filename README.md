# gitee-vendor-dll

[![npm](https://img.shields.io/npm/v/gitee-vendor-dll.svg)](https://gitee.com/gitee-frontend/gitee-vendor-dll/releases)
[![gitee-release-cli](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-gitee--release-blueviolet.svg)](https://gitee.com/gitee-frontend/gitee-reelase-cli/blob/master/LICENSE)

## 目录

<!-- TOC -->

- [gitee-vendor-dll](#gitee-vendor-dll)
    - [目录](#目录)
    - [介绍](#介绍)
    - [使用](#使用)
    - [开发](#开发)
        - [如何添加新的依赖包？](#如何添加新的依赖包)
    - [发布](#发布)

<!-- /TOC -->

## 介绍

码云的第三方依赖包，包含了已预构建的第三方依赖库的资源文件。

## 使用

修改 webpack 配置文件，添加如下代码：

```js
const vendorDll = require('gitee-vendor-dll')

// 创建配置，它会根据当前模式选择开发版或生产版的资源文件
const dllConfig = vendorDll.createConfig('production')

const config = {
  plugins: [
    // 追加合并 gitee-vendor-dll 的 manifest 文件
    new StatsPlugin('manifest.json', { otherPath: dllConfig.manifestPaths }),
    // 将源代码中用到的第三方库链接到 vendors 库中
    new webpack.DllReferencePlugin(dllConfig.library.vendors.referenceConfig),
    // 构建完后复制 gitee-vendor-dll 中预构建的资源文件到输出目录
    new CopyWebpackPlugin([dllConfig.copyConfig])
  ]
}
```

## 开发

```bash
npm install
npx webpack --wath
```

### 如何添加新的依赖包？

假设包名为 mobile_vendors，包含 vue、osc-mint-ui、axios 这三个依赖包，则步骤如下：

1. 编辑 entry.js 文件：
    ```diff
     module.exports = {
    -  vendors: ['vue/dist/vue.esm.js', 'axios', 'vue-timeago', 'highlight.js']
    +  vendors: ['vue/dist/vue.esm.js', 'axios', 'vue-timeago', 'highlight.js'],
    +  mobile_vendors: ['vue', 'axios', 'osc-mint-ui']
     }
    ```
2. 编辑 webpack 配置文件：
    ```diff
       plugins: [
    +    new webpack.DllReferencePlugin(dllConfig.library.mobile_vendors.referenceConfig),
    ```

## 发布

先决条件：

- 已在 [npmjs.org](http://npmjs.org/) 上注册了账号
- 账号已经加入 gitee 组织
- 已用 `npm login` 命令登录了这个账号

之后在命令行中运行：

``` bash
# 创建 beta 预发行版
npm run release-beta

# 发布测试版
npm publish --tag=beta

# 创建正式发行版
npm run release

# 发布正式版
npm publish
```
